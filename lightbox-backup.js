window.StacklaFluidWidgetProperties = {};
StacklaFluidWidgetProperties['4326'] = {
    callbacks: {
        onAfterExpandedTileOpen: function() {
            var isPhone = ($tackla('.stacklapopup-mobile').length && $tackla(document).outerWidth() <= 400),
                $content = $tackla('.stacklapopup-content'),
                $panelLeft = $content.find('.stacklapopup-panel-left'),
                $panelRight = $content.find('.stacklapopup-panel-right'),
                $user = $content.find('.stacklapopup-user-name'),
                $wrapper = $content.find('.stacklapopup-image-wrapper'),
                $inner = $content.find('.stacklapopup-image-wrapper-inner'),
                $caption = $content.find('.stacklapopup-caption'),
                $contentWrapper = $tackla('<div class="stacklapopup-content-wrapper"></div>'),
                classNames = $content.find('.stacklapopup-content-wrap')[0].className.split(' '),
                panelRightHeight = $panelRight.outerHeight(),
                wrapperWidth = $wrapper.width(),
                innerHeight = $inner.height(),
                properHeight = (wrapperWidth > panelRightHeight) ? wrapperWidth : panelRightHeight,
                i, serial, paddingTop;

            // Find the 'label-' tag which presents wine number
            for (i in classNames) {
                if (classNames.hasOwnProperty(i)) {
                    if (classNames[i].indexOf('contains-tag-label-') === 0) {
                        serial = classNames[i].replace('contains-tag-label-', '');
                        break;
                    }
                }
            }

            // Build content
            if (serial) {
                $contentWrapper.append('<div class="stacklapopup-serial">' + serial + '</div>');
            }
            $contentWrapper.append($caption);
            $contentWrapper.append([
                '<div class="stacklapopup-sep"></div>',
                '<div class="stacklapopup-logo"></div>',
                '<div class="stacklapopup-slogan">Numbers Can Be Extraordinary</div>',
                '<div class="stacklapopup-username">' + $user.html() + '</div>'
            ].join(''));
            $panelRight.append($contentWrapper);

            // Centerise content
            paddingTop = ($panelLeft.outerHeight() - $contentWrapper.outerHeight()) / 2 + 'px';

            $panelRight.css('paddingTop', paddingTop);

            // Set height of left pane
            if (isPhone) {
                return;
            }
            $wrapper.css('min-height', properHeight);
            if (innerHeight < properHeight) {
                var diff = properHeight - innerHeight;
                $inner.css('padding-top', diff/2);
            }

            if ($panelLeft.height() > $panelRight.height()) {
                $panelRight.css('height', $panelLeft.height());
            }
        }
    }
};
