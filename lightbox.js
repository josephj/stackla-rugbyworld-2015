(function () {
    window.StacklaFluidWidgetProperties = {};

    var highlightHashtag = function () {
        var $container = $tackla($tackla.find('.stacklapopup-content-wrap')),
            $caption = $container.find('.stacklapopup-caption'),
            message;

        if ($container.hasClass('facebook') || $container.hasClass('instagram')) {
            message = $caption.text();
            message = message.replace(/(#(\w+))/g, '<a href="javascript:void();">$1</a>');
            $caption.html($('<p>' + message + '</p>'));
        }
    };

    var centerize = function () {
        var isPhone = ($tackla('.stacklapopup-mobile').length && $tackla(document.body).width() <= 400),
            $content = $tackla($tackla('.stacklapopup-content')),
            $panelLeft = $content.find('.stacklapopup-panel-left'),
            $panelRight = $content.find('.stacklapopup-panel-right'),
            panelLeftHeight = $panelLeft.height(),
            panelRightHeight = $panelRight.height();

        if (isPhone) {
            return;
        }

        if (panelRightHeight > panelLeftHeight) {
            $panelLeft.css('padding-top', (panelRightHeight - panelLeftHeight) / 2)
            $panelLeft.css('background-color', 'transparent');
        }
    };

    // 4486 - Image Gallery (Vertical)
    StacklaFluidWidgetProperties['4486'] = {
        callbacks: {
            onAfterExpandedTileOpen: function () {
                highlightHashtag();
                centerize();
            }
        }
    };
    // 4503 - Image Gallery (Horizontal)
    StacklaFluidWidgetProperties['4503'] = {
        callbacks: {
            onAfterExpandedTileOpen: function () {
                highlightHashtag();
                centerize();
            }
        }
    };
    // 4534 - Social Hub (Vertical)
    StacklaFluidWidgetProperties['4534'] = {
        callbacks: {
            onAfterExpandedTileOpen: function () {
                highlightHashtag();
                centerize();
            }
        }
    };
    // 4547 - Social Hub (Horizontal)
    StacklaFluidWidgetProperties['4547'] = {
        callbacks: {
            onAfterExpandedTileOpen: function () {
                highlightHashtag();
                centerize();
            }
        }
    };

}());
